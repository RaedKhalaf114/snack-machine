import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {SnackMachineModule} from './snack-machine/snack-machine.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SnackMachineModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
