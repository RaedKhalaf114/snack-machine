import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SnackMachineComponent} from './snack-machine/snack-machine.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'snack-machine',
    pathMatch: 'full'
  },
  {
    path: 'snack-machine',
    component: SnackMachineComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
