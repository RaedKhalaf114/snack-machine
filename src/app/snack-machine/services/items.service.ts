import {Injectable} from '@angular/core';
import {UtilitiesService} from './utilities.service';
import {Observable, Subject} from 'rxjs';
import {ItemModel} from '../models/item.model';
import {flatMap, map} from 'rxjs/operators';
import {PositionModel} from '../models/position.model';
import {CashModel} from '../models/cash.model';

@Injectable()
export class ItemsService {
  onDispenseSnack = new Subject<ItemModel>();
  onDispenseCash = new Subject<CashModel>();

  constructor(
    private utilitiesService: UtilitiesService
  ) {}

  /**
   * Return the Snacks as HashMap.
   * Key: RowColumn -> Value: Item.
   * RowColumn=> the item position.
   */
  public getItems(): Observable<Map<string, ItemModel>> {
    return this.utilitiesService.readFileAsJSON('./assets/datastore/snacks-store')
      .pipe(
        map(({snacks}) => {
          return this.hashSnacks(snacks);
        })
      );
  }

  public updateStore(snacks: ItemModel[]) {
    // TODO.
  }

  /**
   * Convert the array of items to items Map.
   * Key: RowColumn -> Value: Item.
   * RowColumn=> the item position.
   */
  public hashSnacks(snacks: ItemModel[]): Map<string, ItemModel> {
    const snacksMap = new Map<string, ItemModel>();
    snacks.forEach(
      (snack: ItemModel) => {
        const key = `${snack.position.row}${snack.position.column}`;
        snacksMap.set(key, snack);
      }
    );
    return snacksMap;
  }

  /**
   *
   */
  public getItemByPosition(position: PositionModel, snacks: Map<string, ItemModel>) {
    const key = `${position.row}${position.column}`;
    return snacks.get(key);
  }
}
