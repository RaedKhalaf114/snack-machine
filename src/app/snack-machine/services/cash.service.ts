import {Injectable} from '@angular/core';
import {CashModel, CashType} from '../models/cash.model';

@Injectable()
export class CashService {
  private readonly cashTypes = [50, 20, 5, 1, .5, .2, .1];
  public isCashValid(cash: CashModel) {
    // TODO Do Real Verifications.
    if (Object.values(CashType).includes(cash.type)) {
      return true;
    }
    return false;
  }

  /**
   * Return the type of returned cash.
   * ex: return 33$ => one 20$, two 5$, three 1
   * @param amount
   */
  public getCashSchema(amount) {
    const schema = this.buildCashSchema(amount, '');
    return schema.substr(2);
  }

  /**
   * Recursion function for building the schema.
   * @param amount
   * @param schema
   */
  private buildCashSchema(amount: number, schema) {
    // stop point.
    if (amount === 0) {
      return schema;
    }

    // current item schema.
    let cuttedCash;

    this.cashTypes.some(cash => {
      if (cash <= amount) {
        cuttedCash = cash;
        return true;
      }
    });

    if (cuttedCash) {
      return this.buildCashSchema(amount - cuttedCash, schema + ', ' + cuttedCash + '$');
    } else {
      return 'Error..';
    }
  }
}
