import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class UtilitiesService {

  constructor(
    private http: HttpClient
  ) {}

  public readFileAsJSON(path): Observable<any> {
    return this.http.get(path);
  }
}
