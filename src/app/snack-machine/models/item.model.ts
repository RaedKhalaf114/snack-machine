import {PositionModel} from './position.model';

export interface ItemModel {
  name: string;
  price: number;
  quantity: number;
  position: PositionModel;
  imgURL?: string;
}
