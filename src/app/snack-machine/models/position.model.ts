export interface PositionModel {
  row: number;
  column: number;
}
