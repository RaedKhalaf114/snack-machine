export enum CashType {
  TEN_C = .10,
  TWENTY_C = .20,
  FIFTY_C = .50,
  ONE_DOLLOR = 1,
  TENTY_DOLLORS = 20,
  FIFTY_DOLLORS = 50,
  CARDS = -1
}

export interface CashModel {
  type: CashType;
  amount: number;
  label?: string;
}
