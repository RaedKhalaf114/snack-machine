import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-keypad-component',
  templateUrl: 'keypad.component.html',
  styleUrls: ['keypad.component.css']
})
export class KeypadComponent {
  /**
   * Keys to be displayed in the KeyPad.
   */
  public readonly INPUTS = ['1', '2', '3', '4', '5'];

  @Output() keypadTriggered: EventEmitter<number> = new EventEmitter();
  @Input() isBlocked: boolean;

  public onKeypressed(input) {
    if (this.isBlocked) {
      return;
    }

    this.playSound();
    this.keypadTriggered.emit(input);
  }

  private playSound() {
    const audio = new Audio();
    audio.src = 'https://d1490khl9dq1ow.cloudfront.net/sfx/mp3preview/keyboard-click_zJKdUG4O.mp3';
    audio.load();
    audio.play();
  }
}
