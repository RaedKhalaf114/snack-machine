import {Component, OnDestroy, OnInit} from '@angular/core';
import {ItemsService} from '../../services/items.service';
import {ItemModel} from '../../models/item.model';

@Component({
  selector: 'app-snack-dispense-component',
  templateUrl: 'snack-dispense.component.html',
  styleUrls: ['snack-dispense.component.css']
})
export class SnackDispenseComponent implements OnInit, OnDestroy {
  private snackDispenseSubscription;

  public openDispenseDoor = false;
  public dispensedSnack: ItemModel;

  constructor(
    private itemsService: ItemsService
  ) {}

  ngOnInit(): void {
    this.initSnackSubscription();
  }

  ngOnDestroy(): void {
    this.destroySnackSubscription();
  }

  private destroySnackSubscription() {
    if (this.snackDispenseSubscription) {
      this.snackDispenseSubscription.unsubscribe();
    }
  }

  private initSnackSubscription() {
    this.snackDispenseSubscription = this.itemsService.onDispenseSnack
      .subscribe(
        (snack: ItemModel) => {
          this.dispenseSnack(snack);
        }
      );
  }

  public dispenseSnack(snack: ItemModel) {
    this.openDispenseDoor = true;
    this.dispensedSnack = snack;
    this.playSound();

    // clear up, after animation.
    setTimeout(() => {
      this.openDispenseDoor = false;
      this.dispensedSnack = undefined;
    }, 2000);
  }

  private playSound() {
    const audio = new Audio();
    audio.src = 'http://soundbible.com/mp3/Cash Register Cha Ching-SoundBible.com-184076484.mp3';
    audio.load();
    audio.play();
  }
}
