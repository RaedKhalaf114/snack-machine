import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {CashModel, CashType} from '../../models/cash.model';

@Component({
  selector: 'app-cash-collect-component',
  templateUrl: 'cash-collect.component.html',
  styleUrls: ['cash-collect.component.css']
})
export class CashCollectComponent {
  /**
   *
   */
  @Input() disabled;

  /**
   *
   */
  @Output() inputCash: EventEmitter<CashModel> = new EventEmitter();

  public readonly COINS: CashModel[] = [
    {
      type: CashType.TEN_C,
      amount: .1,
      label: 'Ten Cents'
    },
    {
      type: CashType.TWENTY_C,
      amount: .2,
      label: 'Twenty Cents'
    },
    {
      type: CashType.FIFTY_C,
      amount: .5,
      label: 'Fifty Cents'
    },
    {
      type: CashType.ONE_DOLLOR,
      amount: 1,
      label: 'One Dollar'
    },
    {
      type: CashType.TENTY_DOLLORS,
      amount: 20,
      label: 'Twenty Dollar'
    },
    {
      type: CashType.FIFTY_DOLLORS,
      amount: 50,
      label: 'Fifty Dollar'
    },
    {
      type: 52,
      amount: 30,
      label: 'Error Coin'
    }
  ];

  public onInputCash(cash: CashModel) {
    this.playSound();
    this.inputCash.emit(cash);
  }

  private playSound() {
    const audio = new Audio();
    audio.src = 'https://d1490khl9dq1ow.cloudfront.net/sfx/mp3preview/money-drop-short_G18bUzNd.mp3';
    audio.load();
    audio.play();
  }
}
