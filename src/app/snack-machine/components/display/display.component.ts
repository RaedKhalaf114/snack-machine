import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-display-component',
  templateUrl: 'display.component.html',
  styleUrls: ['display.component.css']
})
export class DisplayComponent {
  /**
   * Current displayed msg on the screen.
   */
  @Input() msg = 'Ready...';
}
