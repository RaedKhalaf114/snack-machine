import {Component, Input, OnInit} from '@angular/core';
import {ItemModel} from '../../models/item.model';
import {UtilitiesService} from '../../services/utilities.service';

@Component({
  selector: 'app-items-component',
  templateUrl: 'items.component.html',
  styleUrls: ['items.component.css']
})
export class ItemsComponent {
  /**
   * The list of items available in the Vendor Machine.
   */
  @Input() items: ItemModel[];
}
