import {NgModule} from '@angular/core';
import {SnackMachineComponent} from './snack-machine.component';
import {CommonModule} from '@angular/common';
import {ItemsComponent} from './components/items/items.component';
import {CashCollectComponent} from './components/cash-collect/cash-collect.component';
import {CashDispenseComponent} from './components/cash-dispense/cash-dispense.component';
import {DisplayComponent} from './components/display/display.component';
import {KeypadComponent} from './components/keypad/keypad.component';
import {SnackDispenseComponent} from './components/snack-dispense/snack-dispense.component';
import {ItemsService} from './services/items.service';
import {HttpClientModule} from '@angular/common/http';
import {UtilitiesService} from './services/utilities.service';
import {CashService} from './services/cash.service';

@NgModule({
  declarations: [
    SnackMachineComponent,
    ItemsComponent,
    CashCollectComponent,
    CashDispenseComponent,
    DisplayComponent,
    ItemsComponent,
    KeypadComponent,
    SnackDispenseComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [
    SnackMachineComponent
  ],
  providers: [
    UtilitiesService,
    ItemsService,
    CashService
  ]
})
export class SnackMachineModule {}
