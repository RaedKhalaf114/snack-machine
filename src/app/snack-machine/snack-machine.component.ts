import {Component, OnInit} from '@angular/core';
import {UtilitiesService} from './services/utilities.service';
import {ItemModel} from './models/item.model';
import {PositionModel} from './models/position.model';
import {ItemsService} from './services/items.service';
import {SnackMachineStatusEnum} from './models/snack-machine-status.model';
import {CashModel} from './models/cash.model';
import {CashService} from './services/cash.service';

@Component({
  selector: 'app-snack-machine-component',
  templateUrl: 'snack-machine.component.html',
  styleUrls: ['snack-machine.component.css']
})
export class SnackMachineComponent implements OnInit {
  /**
   * Available Items in the Machine.
   */
  private snacksMap: Map<string, ItemModel>;
  public snacksList: ItemModel[];

  /**
   * Current Status of the VM.
   */
  private status: SnackMachineStatusEnum = SnackMachineStatusEnum.READY;

  /**
   * Track the status Change.
   */
  private lastStatusChange = new Date();

  /**
   * Current Customer Input.
   */
  private customerInput = '';

  /**
   *
   */
  public displayedMsg = '';

  /**
   * the Current Collected Cash.
   */
  private collectedCash = 0;

  /**
   * The price of the selected Snack.
   */
  private selectedSnack: ItemModel;

  /**
   * Enable and disable Keypad according to the status.
   */
  public isKeypadDisabled = false;

  /**
   * Enable/Disable cash Input according to the status.
   */
  public isCashInputDisabled = true;

  /**
   * destroy the previous timoutId when there is new one.
   * timeout is used to reset the machine after Xseconds
   * when there is no user interaction.
   */
  private timeoutId;

  constructor(
    private utilitiesService: UtilitiesService,
    private itemsService: ItemsService,
    private cashService: CashService
  ) {}

  ngOnInit() {
    this.initMachine();
  }

  /**
   * Reset the machine parameters.
   * after finishing process.
   * @param duration: after Xms of time reset it.
   */
  public resetMachine(duration, conditionalDate?) {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
    this.timeoutId = setTimeout(() => {
      if (conditionalDate && this.lastStatusChange !== conditionalDate) {
        return;
      }

      // if there is inserted cash in the machine,
      // dispense it and reset VM.
      if (this.collectedCash > 0) {
        this.dispenseCash(this.collectedCash);
      }

      this.isKeypadDisabled = false;
      this.isCashInputDisabled = true;
      this.displayedMsg = 'Ready...';
      this.customerInput = '';
      this.collectedCash = 0;
      this.selectedSnack = undefined;
      this.status = SnackMachineStatusEnum.READY;
      this.lastStatusChange = new Date();
    }, duration);
  }

  // Initialize Machine Parameters.
  private initMachine() {
    this.initItems();
    this.initDisplayedMsg();
  }

  /**
   * Initialize the Msg displayed in the VM.
   */
  private initDisplayedMsg() {
    this.displayedMsg = 'Ready...';
  }

  /**
   * get Items from Data-store.
   */
  private initItems() {
    this.itemsService.getItems().subscribe(
      (snacks) => {
        this.snacksMap = snacks;
        this.snacksList = Array.from(snacks.values());
      }
    );
  }

  private processOrder() {
    const position = {
      row: +this.customerInput.charAt(0),
      column: +this.customerInput.charAt(1)
    };

    const isSnackAvailable = this.isSnackAvailable(position);

    // if its not available.
    // show `Not Found.` for 3 seconds, then reset the machine.
    if (!isSnackAvailable) {
      this.displayedMsg = 'Not Found.';
      this.resetMachine(3000, this.lastStatusChange);
      return;
    }

    const item = this.itemsService.getItemByPosition(position, this.snacksMap);
    this.displayedMsg = item.price + '$';
    this.status = SnackMachineStatusEnum.WAITING_FOR_MONEY;
    const startWaitingForMoneyMoment = new Date();
    this.lastStatusChange = startWaitingForMoneyMoment;
    this.resetMachine(15000, startWaitingForMoneyMoment);

    this.selectedSnack = item;
    this.isCashInputDisabled = false;
  }

  /**
   *
   */
  private disableKeypad() {
    this.isKeypadDisabled = true;
  }

  /**
   * Check if the Snack Available in the Machine.
   */
  public isSnackAvailable(position: PositionModel) {
    const item = this.itemsService.getItemByPosition(position, this.snacksMap);
    return item.quantity > 0;
  }

  /**
   *
   */
  public dispenseSnack(position: PositionModel, dispenseCash) {
    const item = this.itemsService.getItemByPosition(position, this.snacksMap);
    item.quantity--;
    this.itemsService.updateStore(Array.from(this.snacksMap.values()));
    this.itemsService.onDispenseSnack.next(item);

    if (dispenseCash) {
      setTimeout(() => {
        this.dispenseCash(this.collectedCash);
      }, 3000);
    }
  }

  public dispenseCash(amount) {
    if (this.collectedCash === 0) {
      this.resetMachine(0);
      return;
    }

    if (this.collectedCash < amount) {
      this.status = SnackMachineStatusEnum.ERROR;
      const machineError = new Date();
      this.lastStatusChange = machineError;
      this.resetMachine(5000, machineError);
      return;
    }

    this.displayedMsg = `Return ${Math.round(amount * 10) / 10}$`;
    this.collectedCash -= amount;
    // TODO figure out the the Returned Cash Schema.
    const cashSchema = this.cashService.getCashSchema(amount);

    this.resetMachine(2000);
  }

  /**
   * Process Customer Input.
   */
  public onCustomerInput(inputKey) {
    if (this.customerInput.length !== 1) {
      this.status = SnackMachineStatusEnum.WAITING_FOR_INPUT;
      const customerStartInput = new Date();
      this.lastStatusChange = customerStartInput;
      this.customerInput = inputKey;
      this.displayedMsg = this.customerInput;
      this.resetMachine(5000, customerStartInput);
    } else {
      this.status = SnackMachineStatusEnum.PROCESSING_ORDER;
      this.lastStatusChange = new Date();
      this.customerInput += inputKey;
      this.displayedMsg = this.customerInput;
      this.disableKeypad();
      this.processOrder();
    }
  }

  public onInputCash(cash: CashModel) {
    let isValid = true;
    let errorMsg = '';

    // if Status is not WAITING_FOR_MONEY, don't do anything.
    if (this.status !== SnackMachineStatusEnum.WAITING_FOR_MONEY
      && this.status !== SnackMachineStatusEnum.INSERTING_MONEY) {
      this.dispenseCash(cash.amount);
      return;
    }

    if (!this.cashService.isCashValid(cash)) {
      isValid = false;
      errorMsg = 'Error...';
    }

    this.status = SnackMachineStatusEnum.INSERTING_MONEY;
    const customerStartedInsertingMoney = new Date();
    this.lastStatusChange = customerStartedInsertingMoney;
    this.resetMachine(30000, customerStartedInsertingMoney);
    this.collectedCash += cash.amount;
    this.displayedMsg = `${Math.round(this.collectedCash * 10) / 10}$`;

    // this means the cash inserted is enough to purchase.
    if (this.selectedSnack.price <= this.collectedCash) {
      this.status = SnackMachineStatusEnum.FAINALIZING_ORDER;
    } else {
      // no enough money.
      return;
    }

    if (!this.canReturnMoney()) {
      isValid = false;
      errorMsg = 'No Money..';
    }

    if (!isValid) {
      this.displayedMsg = errorMsg;
      const errorDate = new Date();
      this.lastStatusChange = errorDate;
      this.status = SnackMachineStatusEnum.ERROR;
      this.disableKeypad();
      this.playErrorSound();
      this.isCashInputDisabled = true;
      this.resetMachine(3000, errorDate);
      return;
    }

    this.disableKeypad();
    this.isCashInputDisabled = true;
    this.collectedCash = this.collectedCash - this.selectedSnack.price;
    this.displayedMsg = 'Processing';

    this.dispenseSnack(this.selectedSnack.position, true);
  }

  /**
   * Check if there is enough cash in the machine which
   * can cover the money the VM needs to dispense for the customer.
   */
  private canReturnMoney() {
    // TODO check the local Budget.
    return true;
  }

  private playErrorSound() {
    const audio = new Audio();
    audio.src = 'https://d1490khl9dq1ow.cloudfront.net/sfx/mp3preview/error-buzz_zJ07cu4u.mp3';
    audio.load();
    audio.play();
  }
}
